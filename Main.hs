module Main where 

import Data.Char
import Control.Applicative

-- AST
data JsonValue
  = JsonNull
  | JsonNumber Double
  | JsonString String
  | JsonBool Bool
  | JsonArray [JsonValue]
  | JsonObject [(String, JsonValue)]
  deriving (Show, Eq)

-- Error reporting
data ParserError = ParserError Int String deriving (Show)

-- "Objectify" Parser
newtype Parser a :: Parser
  { runParser :: Input -> Either ParserError (Input, a) }


-- Functor proof for Parser
instance Functor Parser where
  fmap f (Parser p) =
    Parser $ \input -> do
      (input', x) <- p input
      Just (input', f x)

-- Applicative proof for Parser
instance Applicative Parser where
  pure x = Parser $ \input -> Just (input, x)
  (Parser p1) <*> (Parser p2) =
    Parser $ \input -> do
      (input', f) <- p1 input
      (input'', a) <- p2 input'
      Just (input'', f a)

-- Alternative proof for Parser
instance Alternative Parser where
  empty = Parser $ \_ -> Nothing
  (Parser p1) <|> (Parser p2) =
      Parser $ \input -> p1 input <|> p2 input

-- SECTION II: Helper parsers. These can be combined separated by a <|>
-- ex: runParser (jsonNull <|> jsonBool) "false"

-- Helper parser that parsers a char
-- ex: runParser (charP 'o') "object"
charP :: Char -> Parser Char
charP x = Parser f
  where
      f input@(inputUncons -> Just (y, ys))
        | y == x = Right (ys, x)
        | otherwise =
          Left $
          ParserError
            (inputLoc input)
            ("Expected '" ++ [x] ++ "', but found '" ++ [y] ++ "'")
      f input =
        Left $
        ParserError
          (inputLoc input)
          ("Expected '" ++ [x] ++ "', but reached end of string")

-- Helper parser that parsers sequence of characters (strings)
stringP :: String -> Parser String
stringP = sequenceA . map charP -- sequenceA -> Applicative proof -> Functor proof

-- Helper parser that parsers null
jsonNull :: Parser JsonValue
jsonNull = JsonNull <$ stringP "null"

-- Helper parser for non-nulls
notNull :: Parser [a] -> Parser [a]
notNull (Parser p) =
  Parser $ \input -> do
    (input', xs) <- p input
    if null xs
      then Nothing
      else Just (input', xs)

-- Helper parser for numbers
jsonNumber :: Parser JsonValue
jsonNumber = f <$> notNull (spanP isDigit)
    where f ds = JsonNumber $ read ds

-- Helper parser that parsers booleans
jsonBool :: Parser JsonValue
jsonBool = f <$> (stringP "true" <|> stringP "false")
   where f "true"  = JsonBool True
         f "false" = JsonBool False
         -- This should never happen
         f _       = undefined

-- Helper parser that parsers spans
-- ex: runParser (spanP isDigit) 05179107comprehensive
spanP :: (Char -> Bool) -> Parser String
spanP f =
  Parser $ \input ->
    let (token, rest) = span f input
     in Just (rest, token)

stringLiteral :: Parser String
stringLiteral = charP '"' *> spanP (/= '"') <* charP '"'

jsonString :: Parser JsonValue
jsonString = JsonString <$> stringLiteral

ws :: Parser String
ws = spanP isSpace

-- Parser separator injection. Parser a = separator, Parser b = element
sepBy :: Parser a -> Parser b -> Parser [b]
sepBy sep element = (:) <$> element <*> many (sep *> element) <|> pure []

jsonArray :: Parser JsonValue
jsonArray = JsonArray <$> (charP '[' *> ws *>
                           elements
                           <* ws <* charP ']')
  where
    elements = sepBy (ws *> charP ',' <* ws) jsonValue

jsonObject :: Parser JsonValue
jsonObject =
  JsonObject <$> (charP '{' *> ws *> sepBy (ws *> charP ',' <* ws) pair <* ws <* charP '}')
  where
    pair =
      (\key _ value -> (key, value)) <$> stringLiteral <*>
      (ws *> charP ':' <* ws) <*>
      jsonValue

jsonValue :: Parser JsonValue
jsonValue = jsonNull <|> jsonBool <|> jsonNumber <|> jsonString <|> jsonArray <|> jsonObject

-- Accept file, output result
parseFile :: FilePath -> Parser a -> IO (Maybe a)
parseFile fileName parser = do
  input <- readFile fileName
  return (snd <$> runParser parser input)

main :: IO ()
main = undefined